<?php
/**
 * Custom Blocks Loader
 *
 * @package           Block Editor
 * @author            mnap
 * @copyright         2021 mnap
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Blocks Loader
 * Plugin URI:        https://gitlab.com/mnap-playground/block-editor-mu-plugins
 * Description:       Loads custom made blocks for the Gutenberg block editor.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            mnap
 * Author URI:        https://gitlab.com/mnap
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
 * Text Domain:       blked
 * Domain Path:       /languages
 */

define( 'BLKED_MUPLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once BLKED_MUPLUGIN_DIR . 'sample-block/sample-block.php';
